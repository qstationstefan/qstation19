import { useState } from "react";
import CarsList from "./components/cars/CarsList";
import UsersList from "./components/users/UsersList";

function App() {
  const [showCars, setShowCars] = useState(false);
  const [showUsrers, setShowUsers] = useState(false);

  const showCarsHandler = () => {
    setShowCars(prev => !prev)
  }

  const showUsersHandler = () => {
    setShowUsers(prev => !prev)
  }
  return (
    <div>
      <button onClick={showUsersHandler}>Show/hide users</button>
      <button onClick={showCarsHandler}>Show/hide cars</button>
      {showCars && <CarsList />}
      {showUsrers && <UsersList />}
    </div>
  );
}

export default App;
