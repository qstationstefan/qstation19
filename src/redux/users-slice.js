import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    users: []
}
export const usersSlice = createSlice({
    name: "users",
    initialState,
    reducers: {
        addUsers(state, action) {
            state.users = action.payload;
        },
        removeUsers(state, action) {
            state.users = [];
        },
        addSingleUser(state, action) {
            state.users = [action.payload, ...state.users]
        },
        removeUserByEmail(state, action) {
            state.users = state.users.filter(user => user.email !== action.payload);
        }
    }
})

export const { addUsers, removeUsers, addSingleUser, removeUserByEmail } = usersSlice.actions;