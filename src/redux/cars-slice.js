import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cars: [],
};

export const carsSlice =  createSlice({
  name: "cars",
  initialState,
  reducers: {
    addCars(state, action) {
      state.cars = action.payload;
    },
    removeCars(state) {
        state.cars = [];
    }
  },
});


export const { addCars, removeCars } = carsSlice.actions;