import { configureStore } from "@reduxjs/toolkit";
import { carsSlice } from "./cars-slice";
import { usersSlice } from "./users-slice";

export default configureStore({
  reducer: {
    cars: carsSlice.reducer,
    users: usersSlice.reducer,
  },
});
