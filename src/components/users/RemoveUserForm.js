import { useState } from "react";
import { useDispatch } from "react-redux";
import { removeUserByEmail } from "../../redux/users-slice";

const RemoveUserForm = () => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(removeUserByEmail(email));
    setEmail("");
  };

  const mailChangeHandler = (e) => {
    setEmail(e.target.value);
  };
  return (
    <form onSubmit={submitHandler}>
      <input onChange={mailChangeHandler} value={email} />
      <button type="submit">Remove user</button>
    </form>
  );
};

export default RemoveUserForm;
