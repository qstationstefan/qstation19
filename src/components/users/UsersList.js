import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { axiosInstance } from "../../api/axios";
import { addUsers } from "../../redux/users-slice";
import RemoveUserForm from "./RemoveUserForm";
import User from "./User";

const UsersList = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.users);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axiosInstance.get("/users");
        dispatch(addUsers(response.data.Users));
      } catch (error) {
        console.log(error);
      }
    };

    fetchUsers();
  }, [dispatch]);

  return (
    <div>
      <RemoveUserForm />
      {users.map((user) => (
        <User
          key={user.id}
          name={user.first_name}
          surname={user.last_name}
          mail={user.email}
        />
      ))}
    </div>
  );
};

export default UsersList;
