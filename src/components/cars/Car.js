const Car = (props) => {
    return (
        <div>
            <p>Car {props.car} Model {props.model} Year: {props.year}</p>
        </div>
    )
}

export default Car;