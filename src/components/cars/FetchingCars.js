import { useCallback, useEffect, useReducer } from "react";
import { useDispatch } from "react-redux";
import { axiosInstance } from "../../api/axios";
import { addCars, removeCars } from "../../redux/cars-slice";

const initialState = {
  loading: false,
  error: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "LOADING":
      return {
        error: false,
        loading: true,
      };
    case "ERROR":
      return {
        error: true,
        loading: false,
      };
    case "SUCCESS":
      return {
        loading: false,
        error: false,
      };
    default:
      return state;
  }
};

const FetchingCars = () => {
  const [apiState, dispatchReducer] = useReducer(reducer, initialState);

  const dispatch = useDispatch();

  const fetchCars = useCallback(async () => {
    try {
      dispatch(removeCars());
      dispatchReducer({ type: "LOADING" });
      const response = await axiosInstance.get("/cars");
      dispatchReducer({ type: "SUCCESS" });
      dispatch(addCars(response.data.cars));
    } catch (error) {
      dispatchReducer({ type: "ERROR" });
    }
  }, [dispatch]);

  useEffect(() => {
    fetchCars();
  }, [fetchCars]);

  const fetchCarsHandler = () => {
    fetchCars();
  };

  const removeCarsHandler = () => {
    dispatch(removeCars());
  };

  return (
    <div>
      <button onClick={fetchCarsHandler}>Get cars</button>
      <button onClick={removeCarsHandler}>Remove cars</button>
      {apiState.error && <h2>ERROR!</h2>}
      {!apiState.error && apiState.loading && <h2>Loading...</h2>}
    </div>
  );
};

export default FetchingCars;
