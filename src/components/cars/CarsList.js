import { useSelector } from "react-redux";
import Car from "./Car";
import FetchingCars from "./FetchingCars";
const CarsList = () => {
  const cars = useSelector((state) => state.cars.cars);

  return (
    <div>
      <FetchingCars />
      {cars.map((car) => (
        <Car
          key={car.id}
          car={car.car}
          model={car.car_model}
          year={car.car_model_year}
        />
      ))}
    </div>
  );
};

export default CarsList;
